import java.util.*;

class GroupAnagram {
    public List<List<String>> groupAnagrams (String[] strs)
    {
        Map<String,List<String >> groupedAnagrams = new HashMap<String, List<String>>();
        for(String s : strs)
        {
            char[] chr = s.toCharArray();
            Arrays.sort(chr);
            String sortedStr =String.valueOf(chr);

            if(!groupedAnagrams.containsKey(sortedStr))
            {
                groupedAnagrams.put(sortedStr, new ArrayList<String>());
                //System.out.println(groupedAnagrams);
            }
            groupedAnagrams.get(sortedStr).add(s);
        }

        return new ArrayList<List<String>>(groupedAnagrams.values());
    }

}
