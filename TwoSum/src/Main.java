import java.util.*;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        HashMap<Integer,Integer> nums = new HashMap<Integer, Integer>();
        Scanner in = new Scanner(System.in);
        int target, length, index = 0;
        target=in.nextInt();
        length=in.nextInt();
        for(int i=0;i< length;i++ )
        {
            nums.put(i, in.nextInt());
        }


        Set<Integer> key = nums.keySet();

        for(int i=0;i<length;i++)
        {
            int sub = target - nums.get(i);
           // System.out.println(sub);
            if(nums.containsValue(sub) && sub!=nums.get(i) )
            {
               for(Integer k :key)
               {
                   if(sub==nums.get(k))
                   {
                       index=k;
                       break;
                   }
               }
                System.out.println(i+" "+ index);
                break;
            }
        }

    }
}
