import java.util.Arrays;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        String s,t;
        Scanner in= new Scanner(System.in);
        s=in.nextLine();
        t=in.nextLine();
        char[] s1=s.toCharArray();
        char[] t1=t.toCharArray();
        Arrays.sort(s1);
        Arrays.sort(t1);
        System.out.println(Arrays.equals(s1,t1));
    }
}
