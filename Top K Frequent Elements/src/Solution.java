import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class Solution {
    public int[] topKFrequent(int[] nums, int k) {

        HashMap<Integer,Integer> map1 = new HashMap<Integer, Integer>();
        ArrayList<Integer> freqList = new ArrayList<Integer>();
        int[] freqArray= new int[k];

        for(int i=0;i<nums.length;i++)
        {
            if(map1.containsKey(nums[i]))
            {
                map1.put(nums[i],map1.get(nums[i])+1);
                if(map1.get(nums[i])== k)
                    freqList.add(nums[i]);
            }

            else
            {
                map1.put(nums[i],1);
                if(map1.get(nums[i])== k)
                    freqList.add(nums[i]);
            }
        }
        int index=0;

        for(int i: freqList)
        {
            freqArray[index] = i;
            index++;
        }

        return freqArray;
    }
}
