import java.util.*;

public class Main {
    public static void main(String[] args) {
        int[] nums = {1,4,2,1,5,1,3,3};
        Scanner in = new Scanner(System.in);
        int k = in.nextInt();
        Solution s = new Solution();
        int[] newNums = s.topKFrequent(nums,k);
        for (int i =0;i<newNums.length;i++)
        {
            System.out.print(newNums[i]+" ");
        }
    }
}
