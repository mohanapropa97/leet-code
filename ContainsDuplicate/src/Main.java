import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        ArrayList<Integer> nums = new ArrayList<Integer>();
        Scanner in = new Scanner(System.in);
        int length;
        System.out.print("Enter the length of the array: ");
        length=in.nextInt();
        for (int i=0;i<length;i++)
        {
           nums.add(in.nextInt());
        }
        Collections.sort(nums);

        int count=0;
        for(int i=0;i<length-1;i++)
        {
            if( nums.get(i)==nums.get(i+1) )
                count=1;
        }

        if(count==1)
            System.out.println("True");
        else
            System.out.println("False");
    }


}

