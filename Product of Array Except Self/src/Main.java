import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        Solution s = new Solution();
        int[] nums = {-1,1,0,-3,3};

        int[] ans = s.productExceptSelf(nums);

        System.out.print(Arrays.toString(ans));

    }
}
