import java.util.*;

class Solution {
    public boolean isPalindrome(String s) {

        List<Character> list = new ArrayList<Character>();
        s=s.toLowerCase();

        for(int i=0;i<s.length();i++)
        {
            char c = s.charAt(i);
            if((c>='a' && c<='z') || (c>='0'&& c<='9'))
            {
                list.add(c);
            }
        }

      //  System.out.println(list);

        for(int i=0;i<list.size()/2;i++)
        {
            if(list.get(i) != list.get(list.size()-1-i))
                return false;
        }

        return true;

    }
}